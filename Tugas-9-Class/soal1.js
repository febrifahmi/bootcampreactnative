// Rilis 0

class Animal {
    // Code class di sini
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}
 
var sheep = new Animal("shaun");
 
console.log("Soal No 1. Rilis 0 :")
console.log("Sheep name: " + sheep.name) // "shaun"
console.log("No of legs: " + sheep.legs) // 4
console.log("Cold Blooded? " +sheep.cold_blooded) // false

// Rilis 1

// Code class Ape dan class Frog di sini
class Ape extends Animal {
	constructor(name) {
		super(name)
        this.name = name
    }
	yell() {
		return "Auooo";
	}
}

class Frog extends Animal {
	constructor(name) {
		super(name)
        this.name = name
    }
	jump() {
		return "hop hop"
	}
}


 
var sungokong = new Ape("kera sakti")
hasil1 = sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
hasil2 = kodok.jump() // "hop hop"

console.log("Soal No.1 Rilis 1 :")
console.log(hasil1);
console.log(hasil2);
