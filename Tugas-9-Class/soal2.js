// Soal no 2

class Clock {
    // Code di sini
    constructor(template) {
        this.template = template

        var timer;

        this.stop = function() {
			clearInterval(timer);
		};

		this.start = function() {
			this.render();
			timer = setInterval(render, 1000);
		};
    }

    render() {

    	var template;
    	
    	var date = new Date();

	    var hours = date.getHours();
	    if (hours < 10) hours = '0' + hours;

	    var mins = date.getMinutes();
	    if (mins < 10) mins = '0' + mins;

	    var secs = date.getSeconds();
	    if (secs < 10) secs = '0' + secs;

	    var output = template
	      .replace('h', hours)
	      .replace('m', mins)
	      .replace('s', secs);

	    console.log(output);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();
