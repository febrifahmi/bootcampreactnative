// Soal No. 2 Belanja SALE

var sale = {
	SepatuStacattu: 1500000,
	BajuZoro: 500000,
	BajuHnN: 250000,
	SweaterUniklooh: 175000,
	CasingHP: 50000
}

function shoppingTime(memberId, money) {
	listPurchased = [];
	listPurchasedItemName = [];
	purchasedItem = "";
	possiblepurchase = [];
	secondpurchaselist = [];
	leftmoney = "";
	seconditempurchased = "";
	if (memberId == undefined || memberId == "") {
		pesan = "Mohon maaf toko X hanya berlaku untuk member saja";
		return pesan;
	} else if (money <= 50000) {
		pesan = "Mohon maaf, uang tidak cukup";
		return pesan;
	} else {
		for ([key, value] of Object.entries(sale)) {
			if (money > value) {
				// console.log(money+" > "+value);
				possiblepurchase.push(value);
			}
		}
		// console.log(possiblepurchase);
		listPurchased.push(Math.max.apply(Math, possiblepurchase));
		if (listPurchased[0] !== undefined) {
			if (listPurchased[0] == 1500000 ) {
				purchasedItem = "Sepatu Stacattu";	
			} else if (listPurchased[0] == 500000) {
				purchasedItem = "Baju Zoro";
			} else if (listPurchased[0] == 250000) {
				purchasedItem = "Baju H&N";
			} else if (listPurchased[0] == 175000) {
				purchasedItem = "Sweater Uniklooh";
			} else if (listPurchased[0] == 50000) {
				purchasedItem = "Casing HP";
			}
		}
		listPurchasedItemName.push(purchasedItem);
		leftmoney = money - (listPurchased[0]);
		for ([key, value] of Object.entries(sale)) {
			if (leftmoney > value) {
				// console.log(leftmoney+" > "+value);
				secondpurchaselist.push(value);
			}
		}
		// take random SALE item to buy for the second purchase
		seconditempurchased = secondpurchaselist[Math.floor(Math.random() * secondpurchaselist.length)];
		listPurchased.push(seconditempurchased);
		if (seconditempurchased !== undefined) {
			if (seconditempurchased == 1500000 ) {
				purchasedItem = "Sepatu Stacattu";	
			} else if (seconditempurchased == 500000) {
				purchasedItem = "Baju Zoro";
			} else if (seconditempurchased == 250000) {
				purchasedItem = "Baju H&N";
			} else if (seconditempurchased == 175000) {
				purchasedItem = "Sweater Uniklooh";
			} else if (seconditempurchased == 50000) {
				purchasedItem = "Casing HP";
			}
		}
		listPurchasedItemName.push(purchasedItem);
		// console.log(listPurchased)
		var object = {
			memberId: memberId,
			money: money,
			listPurchased: listPurchasedItemName,
			changeMoney: money - listPurchased[0] - listPurchased[1], 
		};
	}
	return object;
}
console.log("Soal No. 2 Belanja SALE: ")
console.log(JSON.stringify(shoppingTime("1820RzKrnWn08",2475000)));
console.log("Test case:")
console.log(JSON.stringify(shoppingTime("82Ku8Ma742",170000)));
console.log(JSON.stringify(shoppingTime('', 2475000)));
console.log(JSON.stringify(shoppingTime('234JdhweRxa53', 15000)));
console.log(JSON.stringify(shoppingTime()));

console.log(JSON.stringify(shoppingTime('321hg3gi1g7', 2750000)));