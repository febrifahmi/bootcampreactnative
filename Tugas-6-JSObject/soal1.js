// Soal No.1 Array to Object
// Subsoal A

var data = [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]];
var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(data){
	for (x = 0; x < data.length; x++){
		// 
		if (thisYear > data[x][3]) {
			birthyear = thisYear - data[x][3];	
		} else {
			birthyear = "Invalid birth year."
		}

		var object = {
			firstName: data[x][0],
			lastName: data[x][1],
			gender: data[x][2],
			age: birthyear,
		}
		// 
		console.log((data.indexOf(data[x])+1)+". "+object.firstName+" "+object.lastName+": "+JSON.stringify(object));
	}
}

arrayToObject(data);

// Subsoal B

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]

console.log("Driver Code test: ");
arrayToObject(people)
arrayToObject(people2)