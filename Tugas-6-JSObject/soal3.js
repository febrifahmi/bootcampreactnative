// Soal No.3 Naik Angkot

inputdata = [['Dimitri', 'B', 'F'], ['Icha', 'A', 'B'], ['Ronald', 'F', 'A']];
results = [];

function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  ongkosperrute = 2000;
  jarak = "";
  //your code here
  for (item in arrPenumpang) {
  	// console.log(arrPenumpang[item]);
  	// console.log("Dari: " + rute.indexOf(arrPenumpang[item][1]));
  	// console.log("Menuju: " + rute.indexOf(arrPenumpang[item][2]));
  	// console.log("Jarak: " + (rute.indexOf(arrPenumpang[item][2])-rute.indexOf(arrPenumpang[item][1])));
  	// console.log("Ongkos: "+ ((rute.indexOf(arrPenumpang[item][2])-rute.indexOf(arrPenumpang[item][1]))*ongkosperrute));
  	if ((rute.indexOf(arrPenumpang[item][2]) > rute.indexOf(arrPenumpang[item][1]))) {
  		jarak = rute.indexOf(arrPenumpang[item][2]) - rute.indexOf(arrPenumpang[item][1]);
  	} else if ((rute.indexOf(arrPenumpang[item][2]) < rute.indexOf(arrPenumpang[item][1]))) {
  		jarak = rute.indexOf(arrPenumpang[item][1]) - rute.indexOf(arrPenumpang[item][2]);
  	}

  	var object = {
	  	penumpang: arrPenumpang[item][0],
	  	naikDari: arrPenumpang[item][1],
	  	tujuan: arrPenumpang[item][2],
	  	bayar: (jarak*ongkosperrute),
	}
	results.push(object);
  }
  return results;
}

console.log("Soal No.3 Naik Angkot")
console.log(JSON.stringify(naikAngkot(inputdata)));