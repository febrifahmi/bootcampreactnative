import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
// import VideoItem from './components/videoItem';
// import data from './data.json';
import LoginScreen from './Tugas/Tugas13/LoginScreen'
import AboutScreen from './Tugas/Tugas13/AboutScreen'
import Main from './Tugas/Tugas14/components/Main'
// import Index from './Tugas/TugasNavigation/index'
import Index from './Quiz3/index'

export default function App() {
  return (
    // <View style={styles.container}>
    //   {/* <LoginScreen></LoginScreen> */}
    //   {/* <AboutScreen></AboutScreen> */}
    //   {/* <Main /> */}
      
    // </View>
    <Index />
  )
}



// export default function App() {
//   return (
//     <View style={styles.container}>
//       <View style={styles.navbar}>
//         <Image source={require('./assets/logo.png')} style={{width:98, height:22,}} />
//         <View style={styles.rightNav}>
//           <TouchableOpacity>
//             <Icon style={styles.navItem} name="search" size={25}></Icon>
//           </TouchableOpacity>
//           <TouchableOpacity>
//             <Icon name="account-circle" size={25}></Icon>
//           </TouchableOpacity>
          
//         </View>
//       </View>

//       <View style={styles.body}>
//         <FlatList 
//           data={data.items}
//           renderItem={(video) => <VideoItem video={video.item} />}
//           keyExtractor={(item) => item.id}
//           ItemSeparatorComponent={() => <View style={{height: 0.5, backgroundColor: "#cccccc"}}/>}
//         />
//       </View>

//       <View style={styles.tabBar}>
//         <TouchableOpacity style={styles.tabItem}>
//           <Icon name="home" size={25}></Icon>
//           <Text style={styles.tabTitle}>Home</Text>
//         </TouchableOpacity>
//         <TouchableOpacity style={styles.tabItem}>
//           <Icon name="whatshot" size={25}></Icon>
//           <Text style={styles.tabTitle}>Trending</Text>
//         </TouchableOpacity>
//         <TouchableOpacity style={styles.tabItem}>
//           <Icon name="subscriptions" size={25}></Icon>
//           <Text style={styles.tabTitle}>Subscribe</Text>
//         </TouchableOpacity>
//         <TouchableOpacity style={styles.tabItem}>
//           <Icon name="folder" size={25}></Icon>
//           <Text style={styles.tabTitle}>Library</Text>
//         </TouchableOpacity>
//       </View>
//     </View>
//   );
// }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  navbar: {
    height: 55,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rightNav: {
    flexDirection: 'row',
  },
  navItem: {
    marginLeft: 25,
    paddingHorizontal: 25,
  },
  body: {
    flex: 1,  
  },
  tabBar: {
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around',
    elevation: 3,
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center',
  },  
  tabTitle: {
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 2,
  }
});
