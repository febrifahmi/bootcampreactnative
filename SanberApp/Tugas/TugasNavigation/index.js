import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { SignIn, CreateAccount, Profile, Search, Home, Details, Search2 } from './Screen'
import { StyleSheet, Text, View } from "react-native";
import {createDrawerNavigator} from '@react-navigation/drawer';

const AuthStack = createStackNavigator();
const Tab = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();
const ProfileStack = createStackNavigator();

const HomeStackScreen = () => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name="Home" component={Home} />
      <HomeStack.Screen name="Details" component={Details} options={({ route }) => ({
        title: route.params.name
      })}/>
    </HomeStack.Navigator>
  )
};

const SearchStackScreen = () => {
  return (
    <SearchStack.Navigator>
      <SearchStack.Screen name="Search" component={Search} />
      <SearchStack.Screen name="Search2" component={Search2} />
    </SearchStack.Navigator>
  )
};

const ProfileStackScreen = () => {
  return(
    <ProfileStack.Navigator>
      <ProfileStack.Screen name="Profile" component={Profile} />
    </ProfileStack.Navigator>
  )
}

const TabScreen = () => {
  return(
    <Tab.Navigator>
        <Tab.Screen name="Home" component={HomeStackScreen} options={{ title: 'Home' }} />
        <Tab.Screen name="Search" component={SearchStackScreen} options={{ title: 'Search' }} />
      </Tab.Navigator>
  )
};

const Drawer = createDrawerNavigator();



export default function Index() {
  return (
    <NavigationContainer>
      <Drawer.Navigator>
        <Drawer.Screen name="Home" component={TabScreen} />
        <Drawer.Screen name="Profile" component={ProfileStackScreen} />
      </Drawer.Navigator>

      {/* <AuthStack.Navigator>
        <AuthStack.Screen name="SignIn" component={SignIn} options={{ title: 'Sign In'}}/>
        <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={{ title: 'Create Account'}}/>
        <AuthStack.Screen name="Home" component={Home} options={{ title: 'Home'}}/>
      </AuthStack.Navigator> */}
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});