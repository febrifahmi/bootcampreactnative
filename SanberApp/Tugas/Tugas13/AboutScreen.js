import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';

const AboutScreen = () => {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.titleTeks}>SanberApp</Text>
            </View>
            <View style={styles.body}>
                <View style={styles.bodyHead}>
                    <Image source={{uri: "https://pbs.twimg.com/profile_images/2661212504/dc29df654d8957df954db40db2a50d7f_400x400.jpeg"}} style={{ height: 120, width: 120, borderRadius: 60}}/>
                    <View style={styles.atasBawah}>
                        <Text style={styles.name}>febrifahmi</Text>
                        <View style={styles.jejeran}>
                            <Icon name='location-pin' size={15} style={styles.iconLocation}/>
                            <Text style={styles.locTeks}>San Fransisco (SF)</Text>
                        </View>
                        <View style={styles.jejeran}>
                            <Text style={{marginTop:15, fontSize:12}}>Programmer at </Text>
                            <Image source={{uri: "https://assets.stickpng.com/images/580b57fcd9996e24bc43c51f.png"}} style={{width: 50, height:15, marginLeft: 5, marginTop: 15}}/>
                        </View>
                    </View>
                </View>
                <View style={styles.bodyBody}>
                    <View style={styles.bodyBodyAtas}>
                        <TouchableOpacity><Image source={{uri: "https://assets.stickpng.com/images/580b57fcd9996e24bc43c53e.png"}} style={styles.socmedLogo} /></TouchableOpacity>
                        <TouchableOpacity><Image source={{uri: "https://assets.stickpng.com/images/580b57fcd9996e24bc43c521.png"}} style={styles.socmedLogo} /></TouchableOpacity>
                        <TouchableOpacity><Image source={{uri: "https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png"}} style={styles.socmedLogo} /></TouchableOpacity>
                    </View>
                    <View style={styles.bodyBodyBawah}>
                        <Text style={styles.titleSkill}>Skills</Text>
                        <View style={styles.LogoSkills}>
                            <View style={styles.innerLogoSkill}>
                                <Image source={{uri: "https://assets.stickpng.com/images/5848152fcef1014c0b5e4967.png"}} style={styles.imageLogoSkill}/>
                                <Text>Python</Text>
                            </View>
                            <View style={styles.innerLogoSkill}>
                                <Image source={{uri: "https://res.cloudinary.com/practicaldev/image/fetch/s--fU-RKnuZ--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/http://1selfsolutions.com/wp-content/uploads/2018/07/flask-preview-400.jpg"}} style={styles.imageLogoSkill}/>
                                <Text>Flask</Text>
                            </View>
                            <View style={styles.innerLogoSkill}>
                                <Image source={{uri: "https://assets.stickpng.com/images/584830f5cef1014c0b5e4aa1.png"}} style={styles.imageLogoSkill}/>
                                <Text>React</Text>
                            </View>
                        </View>
                        <View style={styles.LogoSkills}>
                            <View style={styles.innerLogoSkill}>
                                <Image source={{uri: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bootstrap_logo.svg/1024px-Bootstrap_logo.svg.png"}} style={styles.imageLogoSkill}/>
                                <Text>Bootstrap</Text>
                            </View>
                            <View style={styles.innerLogoSkill}>
                                <Image source={{uri: "https://upload.wikimedia.org/wikipedia/commons/6/6a/JavaScript-logo.png"}} style={styles.imageLogoSkill}/>
                                <Text>Javascript</Text>
                            </View>
                            <View style={styles.innerLogoSkill}>
                                <Image source={{uri: "https://cdn4.iconfinder.com/data/icons/redis-2/1451/Untitled-2-512.png"}} style={styles.imageLogoSkill}/>
                                <Text>Redis</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
            <View style={styles.tabBarBottom}>

            </View>
        </View>
    )
}

export default AboutScreen

const styles = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    header: {
        height: 80,
        width: '100%',
        backgroundColor: "#28B463",
        justifyContent: 'center',
        alignItems: 'center'
    },
    body: {
        flex: 1,
        width: "100%",
    }, 
    bodyHead:{
        flex: 1,
        backgroundColor: "white",
        borderBottomColor: "#D6DBDF",
        borderBottomWidth: 0.1,
        paddingTop: 50,
        paddingHorizontal: 30,
        flexDirection: "row",
        elevation: 2,
        shadowColor: "white",
    },
    bodyBody: {
        flex: 5,
        backgroundColor: "white"
    },
    tabBarBottom: {
        height: 50,
        width: '100%',
        backgroundColor: "#D6DBDF",
    },
    titleTeks: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        marginTop: 40,
    },
    name: {
        color: '#3498DB',
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        marginTop: 20,
    },
    iconLocation: {
        color:"#D6DBDF",
        marginHorizontal: 5,
    },
    atasBawah: {
        flexDirection: "column",
        marginLeft: 40,
    },
    jejeran: {
        flexDirection: "row",
        alignItems: "center"
    },
    locTeks: {
        color: '#D6DBDF',
        fontSize: 12,
        fontFamily: 'Roboto',
    },
    bodyBodyAtas: {
        flexDirection: "row",
        flex: 1,
        paddingRight: 80,
        paddingTop: 10,
        backgroundColor: "white",
        justifyContent: "flex-end"
    },
    socmedLogo:{
        margin: 5,
        width:30, 
        height: 30
    },  
    bodyBodyBawah: {
        flex: 5,
    },
    titleSkill: {
        fontSize: 16,
        fontFamily: "Roboto",
        fontWeight: "bold",
        color: "#5D6D7E",
        paddingHorizontal: 30,
    },
    LogoSkills: {
        flexDirection: "row",
        marginHorizontal: 30,
        justifyContent: "space-around",
    },
    innerLogoSkill: {
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "center",
        margin: 15,
    },
    imageLogoSkill: {
        width: 60,
        height: 60
    },
})
