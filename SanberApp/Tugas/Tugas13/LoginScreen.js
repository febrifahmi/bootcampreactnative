import React from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native'

const LoginScreen = () => {
    return (
        <View style={styles.container}>
            <View style={styles.bgLayer}>
                <Text style={styles.titleLogo}>SanberApp</Text>
                <Image source={{uri: "https://www.freeiconspng.com/thumbs/monkey-png/monkey-png-24.png"}} style={styles.imageLogo}/>
                <Text style={styles.titleLogoSm}>Programming is fun..</Text>
            </View>
            <View style={styles.fgLayer}>
                <Text style={styles.messageSplash}>Ready to show off your code?</Text>
                <View style={styles.loginForm}>
                    <TextInput style={styles.inputTeks}>email</TextInput>
                    <TextInput style={styles.inputTeks}>password</TextInput>
                    <View style={styles.signInSignUp}>
                        <Text style={styles.textSignup}>SignUp</Text>
                        <TouchableOpacity style={styles.button}><Text style={styles.textSignIn}>SignIn</Text></TouchableOpacity>
                    </View>
                    
                </View>
            </View>
        </View>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: '#28B463',
        flex: 1,
    },
    fgLayer: {
        backgroundColor: 'white',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        flex: 1,
    },
    bgLayer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        flex: 2
    },
    titleLogo: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        margin: 15,
    },
    titleLogoSm: {
        color: 'white',
        fontSize: 14,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        margin: 15,
    },
    imageLogo: {
        width: 120,
        height: 120,
    },
    messageSplash: {
        color: '#196F3D',
        fontSize: 16,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        marginLeft: 30,
        marginVertical: 20,
    },
    inputTeks: {
        color: "#D6DBDF",
        borderColor: "gray",
        elevation: 2,
        marginHorizontal: 20,
        marginVertical: 5,
        paddingHorizontal: 20,
        width: 350,
        height: 40,
    },
    loginForm: {
        flex: 1,
        flexDirection: 'column',
    },
    button: {
        backgroundColor: "#28B463",
        width: 0,
        height: 40,
        borderRadius: 10,
        marginRight: 10,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    textSignup: {
        marginVertical: 20,
        color: '#196F3D',
        fontSize: 16,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        flex: 2,
    },
    signInSignUp: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 30,
    },
    textSignIn: {
        color: 'white',
        fontSize: 15,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
    }
})
