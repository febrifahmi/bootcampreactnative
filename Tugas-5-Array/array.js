
// Soal No. 1: Range

// Code di sini
function range(startNum, finishNum) {
	if (startNum === undefined || finishNum === undefined) {
		hasil = [];
		hasil.push(-1);
		return hasil;
	} else if (startNum > finishNum) {
		hasil = [];
		for (i = startNum; i >= finishNum; i--) {
			hasil.push(i);
		}
		return hasil;
	} else if (startNum < finishNum) {
		hasil = [];
		for (i = startNum; i <= finishNum; i++) {
			hasil.push(i);
		}
		return hasil;
	}
}
 
console.log("\nSoal No. 1: Range\n")
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

// Soal No.2: Range with step

// Code di sini
function rangeWithStep(startNum, finishNum, step) {
	if (startNum === undefined || finishNum === undefined || step === undefined) {
		hasil = [];
		hasil.push(-1);
		return hasil;
	} else if (startNum < finishNum) {
		hasil = [];
		for (i=startNum;i<finishNum;i+=step) {
			hasil.push(i);
		}
		return hasil;
	} else if (startNum > finishNum) {
		hasil = [];
		for (i=startNum;i>finishNum;i-=step) {
			hasil.push(i)
		}
		return hasil;
	}
} 

console.log("\nSoal No.2: Range with step\n")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

// Soal No.3 Sum of Range

// Code di sini
function sum(startNum, finishNum, step) {
	if (startNum === undefined && finishNum === undefined && step === undefined) {
		hasil = 0;
		return hasil;
	} else if (startNum && finishNum && step === undefined) {
		step = 1;
		if (startNum < finishNum) {
			hasil = [];
			for (i=startNum;i<finishNum;i+=step) {
				hasil.push(i);
			}
			jumlah = hasil.reduce(function(a, b){
		        return a + b;
		    });
			return jumlah;
		} else if (startNum > finishNum) {
			hasil = [];
			for (i=startNum;i>finishNum;i-=step) {
				hasil.push(i);
			}
			jumlah = hasil.reduce(function(a, b){
		        return a + b;
		    });
			return jumlah;
	} else if (startNum && finishNum && step) {
		if (startNum < finishNum) {
			hasil = [];
			for (i=startNum;i<finishNum;i+=step) {
				hasil.push(i);
			}
			jumlah = hasil.reduce(function(a, b){
		        return a + b;
		    });
			return jumlah;
		} else if (startNum > finishNum) {
			hasil = [];
			for (i=startNum;i>finishNum;i-=step) {
				hasil.push(i);
			}
			jumlah = hasil.reduce(function(a, b){
		        return a + b;
		    });
			return jumlah;
	} else if (startNum === undefined || finishNum === undefined || step === undefined) {
		hasil = [1];
		return hasil;
	}
	
}}};

console.log("\nSoal No.3 Sum of Range\n")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

// Soal No.4 Array Multidimensi

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

// Code kamu di sini, lakukan looping terhadap input di atas
panjanginput = input.length
hasil = ""

function dataHandling(item){
for (var i = 0; i < panjanginput; i++) {
	    hasil += "Nomor ID: " + item[i][0] + "\n" + "Nama Lengkap: " + item[i][1] + "\n" + "TTL: " + item[i][2] + ", " + item[i][3] + "\n" + "Hobi: " + item[i][4] + "\n\n"
	}
	console.log(hasil)
	return hasil
}

var jawaban2 = dataHandling(input)// =  Isikan outputnya di sini

console.log("\nSoal No.4 Array Multidimensi\n")
console.log(jawaban2)

// Soal 5. Balik kata
// Create prompt
function balikString(kal){
    panjangkal = kal.length;
    hasil1 = ""
    for (var i = panjangkal-1; i >= 0; i--){
        hasil1 += kal[i]
    }
    // console.log(hasil1)
    return hasil1
}

const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});

console.log("\nSoal No.5 Membalik kata\n")
readline.question('Masukkan kata/kalimat yang ingin dibalik: ', kata => {
	hasil = balikString(`${kata}`)
	console.log("Hasil: "+hasil);
	readline.close();
});