// Untuk dieksekusi di nodejs
// Tugas 1: menggabung kalimat
var soal1 = "\n#1. Soal 1:\n"
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var sentence = word + ' ' + second + ' ' + third + ' ' +  fourth + ' ' +  fifth + ' ' +  sixth + ' ' + seventh;

console.log(soal1)
console.log(sentence)

// Tugas 2: memecah kalimat
var soal2 = "\n#2. Soal 2:\n"
var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0]; 
var exampleSecondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence.slice(5,10); // lakukan sendiri 
var fourthWord = sentence.slice(11,13); // lakukan sendiri 
var fifthWord = sentence.slice(14,16); // lakukan sendiri 
var sixthWord = sentence.slice(17,22); // lakukan sendiri 
var seventhWord = sentence.slice(23,29); // lakukan sendiri 
var eighthWord = sentence.slice(30,39); // lakukan sendiri

console.log(soal2)
console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)


// Soal No. 3  Mengambil sebagian dari string dengan metode substring
var soal3 = "\n#3. Soal 3:\n"
var kalimat3 = 'wow JavaScript is so cool';
var contohKataPertama3 = kalimat3.substring(0, 3);
var kataKedua3 = kalimat3.substring(4,14); // = Tambahkan sendiri di sini!
var kataKetiga3 = kalimat3.substring(15,17);// = Tambahkan sendiri di sini!
var kataKeempat3 = kalimat3.substring(18,20);// = Tambahkan sendiri di sini!
var kataKelima3 = kalimat3.substring(21,25);// = Tambahkan sendiri di sini!

console.log(soal3)
console.log('First Word: ' + contohKataPertama3); 
console.log('Second Word: ' + kataKedua3); 
console.log('Third Word: ' + kataKetiga3); 
console.log('Fourth Word: ' + kataKeempat3); 
console.log('Fifth Word: ' + kataKelima3);

// Soal 4
var soal4 = "\n#4. Soal 4:\n"
var kalimat4 = 'wow JavaScript is so cool';
var contohKataPertama4 = kalimat4.substring(0, 3);
var kataKedua4 = kalimat4.substring(4,14);// = Tambahkan sendiri di sini!
var kataKetiga4 = kalimat4.substring(15,17);// = Tambahkan sendiri di sini!
var kataKeempat4 = kalimat4.substring(18,20);// = Tambahkan sendiri di sini!
var kataKelima4 = kalimat4.substring(21,25);// = Tambahkan sendiri di sini!

var panjangKataPertama4 = contohKataPertama4.length; // output nya panjang string "wow" adalah 3
var panjangKataKedua4 = kataKedua4.length;// Buat variabel baru di sini untuk mendapatkan panjang dari kata selanjutnya : panjangKataKedua4, panjangKataKetiga4, panjangKataKeempat4, panjangKataKelima4
var panjangKataKetiga4 = kataKetiga4.length;
var panjangKataKeempat4 = kataKeempat4.length;
var panjangKataKelima4 = kataKelima4.length;

console.log(soal4)
console.log("First Word : " + contohKataPertama4 + ', with length: ' + panjangKataPertama4);
console.log("Second Word : " + kataKedua4 + ', with length: ' + panjangKataKedua4);
console.log("Third Word : " + kataKetiga4 + ', with length: ' + panjangKataKetiga4);
console.log("Fourth Word : " + kataKeempat4 + ', with length: ' + panjangKataKeempat4);
console.log("Fifth Word : " + kataKelima4 + ', with length: ' + panjangKataKelima4);