// Untuk dieksekusi di nodejs
// create prompt in node js
const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

console.log("\nCara Bermain:\n");
console.log("1. Tuliskan namamu di form 'Nama'");
console.log("2. Pilih peranmu. Kamu dapat memilih peran sebagai: 'Penyihir','Werewolf', atau 'Guard'");
console.log("3. Mulai permainan")

rl.question("Siapa namamu ? ", function(name) {
    rl.question("Apa peran yang kamu pilih ? ", function(role) {
    	if( `${name}` == "" && `${role}` == ""){
                hasil = "Nama harus diisi!";
                console.log(hasil)
            } else if ( `${name}`.length != 0 && `${role}`.length == 0 ) {
                hasil = "Halo " + `${name}` + ", Pilih peranmu untuk memulai game!";
                console.log(hasil)
            } else if ( `${name}`.length != 0 && `${role}` == "Penyihir" ) {
                hasil1 = "Selamat datang di Dunia Werewolf, " + `${name}` + ".";
                hasil2 = "Halo Penyihir " + `${name}` + ", kamu dapat melihat siapa yang menjadi Werewolf!";
                hasil = hasil1 + "\n" + hasil2;
                console.log(hasil)
            } else if ( `${name}`.length != 0 && `${name}` == "Werewolf" ) {
                hasil1 = "Selamat datang di Dunia Werewolf, " + `${name}` + ".";
                hasil2 = "Halo Werewolf " + `${name}` + ", kamu dapat memilih siapa yang akan kamu makan malam ini!";
                hasil = hasil1 + "\n" + hasil2;
                console.log(hasil)
            } else if ( `${name}`.length != 0 && `${role}` == "Guard" ) {
                hasil1 = "Selamat datang di Dunia Werewolf, " + `${name}` + ".";
                hasil2 = "Halo Guard " + `${name}` + ", Kamu sukses menjadi Guard.";
                hasil = hasil1 + "\n" + hasil2;
                console.log(hasil)
            } else {
                hasil1 = "\nPilih peranmu sesuai aturan main.\nCara Bermain:";
                hasil2 = "1. Tuliskan namamu di form 'Nama'";
                hasil3 = "2. Pilih peranmu. Kamu dapat memilih peran sebagai: 'Penyihir','Werewolf', atau 'Guard'";
                hasil4 = "3. Klik 'Mulai permainan'";
                hasil = hasil1 + "\n" + hasil2 + "\n" + hasil3 + "\n" + hasil4;
                console.log(hasil)
            }
        // console.log(``${name}`, is a citizen of `${role}``);
        rl.close();
    });
});

rl.on("close", function() {
    console.log("\nBYE BYE !!!");
    process.exit(0);
});