// Untuk dieksekusi di nodejs
// eksekusi dengan format "$ node <script>.js tanggal bulan tahun"

if (process.argv[2] === undefined || process.argv[3] === undefined || process.argv[4] === undefined) {
	console.log("Masukkan tanggal bulan tahun dalam format '$> node <script>.js tanggal bulan tahun' dalam angka pada saat menjalankan script ini:")
} else {
	var tanggal = process.argv[2]; // input one
	var bulan = process.argv[3]; // input two
	var tahun = process.argv[4]; // input three

	function cektanggal(tanggal) {
		if (tanggal > 0 && tanggal <= 31) {
			tanggal = tanggal;
			return tanggal;
		} else {
			console.log("Tanggal invalid. Ulangi.")
		}
	}

	function cekbulan(bulan) {
		switch(bulan) {
	    	case "1":
	    		bulan = "Januari";
	    		return bulan;
	    		break;
	    	case "2":
	    		bulan = "Februari";
	    		return bulan;
	    		break;
	    	case "3":
	    		bulan = "Maret";
	    		return bulan;
	    		break;
	    	case "4":
	    		bulan = "April";
	    		return bulan;
	    		break;
	    	case "5":
	    		bulan = "Mei";
	    		return bulan;
	    		break;
	    	case "6":
	    		bulan = "Juni";
	    		return bulan;
	    		break;
	    	case "7":
	    		bulan = "Juli";
	    		return bulan;
	    		break;
	    	case "8":
	    		bulan = "Agustus";
	    		return bulan;
	    		break;
	    	case "9":
	    		bulan = "September";
	    		return bulan;
	    		break;
	    	case "10":
	    		bulan = "Oktober";
	    		return bulan;
	    		break;
	    	case "11":
	    		bulan = "November";
	    		return bulan;
	    		break;
	    	case "12":
	    		bulan = "Desember";
	    		return bulan;
	    		break;
			default:  
				// code block
			    console.log("Bulan invalid. Ulangi");
	    }
	}

	function cektahun(tahun) {
		if (tahun > 0) {
			tahun = tahun;
			return tahun;
		} else {
			console.log("Tahun invalid. ulangi");
		}
	}

	if (cektanggal && cekbulan && cektahun) {
		tanggal = cektanggal(tanggal);
		bulan = cekbulan(bulan);
		tahun = cektahun(tahun);
		console.log(tanggal+" "+bulan+" "+tahun);
	}
}


