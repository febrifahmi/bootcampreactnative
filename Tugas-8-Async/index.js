// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
const kembali = () => {
	console.log("Selesai.");
}

// books.forEach(item => readBooks(10000, item, (callback) => {
//   console.log(callback)
// }))

function startReading(books) {
	for (item in books) {
		// console.log(JSON.stringify(books[item]));
		readBooks(10000,books[item],(kembali) =>{
			console.log(kembali);
		})
	}
}

startReading(books);