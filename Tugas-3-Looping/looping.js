// Untuk dieksekusi dengan node js
// Soal No. 1 Looping menggunakan While
/* 
    Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan menggunakan syntax while. Untuk membuat tantangan ini lebih menarik, kamu juga diminta untuk membuat suatu looping yang menghitung maju dan menghitung mundur. Jangan lupa tampilkan di console juga judul 'LOOPING PERTAMA' dan 'LOOPING KEDUA'." 

    OUTPUT :

    LOOPING PERTAMA
    2 - I love coding
    4 - I love coding
    6 - I love coding
    8 - I love coding
    10 - I love coding
    12 - I love coding
    14 - I love coding
    16 - I love coding
    18 - I love coding
    20 - I love coding
    LOOPING KEDUA
    20 - I will become web developer
    18 - I will become web developer                                                                              
    16 - I will become web developer
    14 - I will become web developer
    12 - I will become web developer
    10 - I will become web developer
    8 - I will become web developer
    6 - I will become web developer
    4 - I will become web developer
    2 - I will become web developer
    */
var jawaban1 = "";
// Code kamu di sini, lakukan looping dengan while

juduljwbn1 = "LOOPING PERTAMA";
juduljwbn2 = "LOOPING KEDUA";

var x = 2;
var y = 20;

while ( x <=20 ){
    if ( x == 2 ){ 
        jawaban1 = juduljwbn1
    }
    jawaban1 += "\n" + x + " - I love coding";
    x += 2;
}

while ( y > 2 ){
    if ( y == 20 ){ 
        jawaban1 += "\n\n" + juduljwbn2
    }
    jawaban1 += "\n" + y + " - I will become web developer";
    y -= 2;
}
console.log("\nSoal No. 1 Looping menggunakan While: \n")
console.log(jawaban1)
console.log("\n\n")

// Soal No. 2
/* 
    Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan menggunakan syntax for. Untuk membuat tantangan ini lebih menarik, kamu juga diminta untuk memenuhi syarat tertentu yaitu: 

    SYARAT: 
    A. Jika angka ganjil maka tampilkan Santai
    B. Jika angka genap maka tampilkan Berkualitas
    C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding. 

    OUTPUT 
    1 - Santai
    2 - Berkualitas
    3 - I Love Coding 
    4 - Berkualitas
    5 - Santai
    6 - Berkualitas
    7 - Santai
    8 - Berkualitas
    9 - I Love Coding
    10 - Berkualitas
    11 - Santai
    12 - Berkualitas
    13 - Santai
    14 - Berkualitas
    15 - I Love Coding
    16 - Berkualitas
    17 - Santai
    18 - Berkualitas
    19 - Santai
    20 - Berkualitas
    */      

var jawaban2 = "";
// Code kamu di sini, lakukan looping dengan syntax for
for (var i = 1; i <= 20; i++) {
    if ( i%2 == 0 ) {
        jawaban2 += i + " - Santai" + "\n"
    } else if ( i%2 != 0 && i%3 != 0) {
        jawaban2 += i + " - Berkualitas" + "\n"
    } else if ( i%2 != 0 && i%3 == 0) {
        jawaban2 += i + " - I Love Coding" + "\n"
    } else {
    }
    
}

console.log("Soal No. 2 Santai Bersama: \n")
console.log(jawaban2)
console.log("\n\n")

// Soal No. 3 Membuat persegi panjang
var jawaban3 = "";

for (var i=0; i <=4; i++) {
    for (var k=0; k <=7; k++) {
        jawaban3 += "#";
    }
    jawaban3 += "\n";
}

console.log("Soal No. 3 Membuat persegi panjang: \n")
console.log(jawaban3)
console.log("\n\n")

// Soal No. 4 Membuat piramid/tangga
var jawaban4 = "";
for (var i = 0; i < 5; i++) {
    for (var k = 0; k <= i; k++){
        jawaban4 += "#";
    };
    jawaban4 += "\n";
}

console.log("Soal No. 4 Membuat tangga/piramid: \n")
console.log(jawaban4)
console.log("\n\n")

// Soal No. 5 Membuat Papan Catur
var jawaban5 = ""
for (var i=1; i <=8; i++) {
    for (var k=1; k <=8; k++) {
        if (k%2 === 0 && i%2 !== 0) {
            jawaban5 += "#";
        } else if (k%2 !== 0 && i%2 === 0) {
            jawaban5 += "#";
        } else {
            jawaban5 += " ";
        }
    }
    jawaban5 += "\n";
}

console.log("Soal No. 5 Membuat checker board: \n")
console.log(jawaban5)
console.log("\n\n")
