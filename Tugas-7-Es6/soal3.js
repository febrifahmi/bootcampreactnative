// Soal No. 3 Destructuring in ES6
// dapatkan data hanya dalam 1 baris saja

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

// jawaban:
const {firstName, lastName, destination, occupation, spell} = newObject;

// Driver code
console.log("Soal No. 3 Destructuring in ES6: ")
console.log(firstName, lastName, destination, occupation)