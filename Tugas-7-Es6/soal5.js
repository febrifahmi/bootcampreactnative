// Soal No.5 Template literals

const planet = "earth"
const view = "glass"
// var before = 'Lorem ' + view + 'dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'

var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod temporincididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log("Soal No.5 Template literals: ")
console.log(before)