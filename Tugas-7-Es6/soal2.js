// Soal No.2 Object literal di ES6

fullName = "";

const newFunction = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName() {
      return console.log(firstName + " " + lastName);
    }
  }
}
 
//Driver Code
console.log("Soal No.2 Object literal di ES6: ") 
newFunction("William", "Imoh").fullName()
newFunction("Rano", "Karno").fullName()
newFunction("Bruce", "Lee").fullName()
newFunction("Jackie", "Chen").fullName()