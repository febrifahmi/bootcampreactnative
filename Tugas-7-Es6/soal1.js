// Soal No. 1 Arrow Function

/* ------- Ubah menjadi bentuk arrow function:

const golden = function goldenFunction(){
  console.log("this is golden!!")
}
 
golden()
----------- */

const golden = goldenFunction = () => {
    //isi function
    console.log("this is golden!!");
} 

golden();