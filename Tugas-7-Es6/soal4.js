// Soal No.4 Spreading

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
// const combined = west.concat(east)

let combined = [...west, ...east];

//Driver Code
console.log(combined)